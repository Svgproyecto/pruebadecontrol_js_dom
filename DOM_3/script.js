'use strict';

const body = document.querySelector('body')
const div = document.createElement('div')

const head = document.querySelector('head')
const style = document.createElement('link')
style.setAttribute('rel', 'stylesheet')
style.setAttribute('href', './style.css')
head.appendChild(style)


function getTime() {

let momentoActual = new Date()
let hora = momentoActual.getHours()
let minutos = momentoActual.getMinutes()
let segundos = momentoActual.getSeconds()

if (hora < 10) {
    hora = '0' + hora
}
if (minutos < 10) {
    minutos = '0' + minutos
}
if (segundos < 10) {
    segundos = '0' + segundos
}


let imprimir = `${hora} : ${minutos} : ${segundos}`

div.innerHTML=` ${imprimir}`
body.appendChild(div) 



setInterval('getTime()', 1000)
}
getTime()





