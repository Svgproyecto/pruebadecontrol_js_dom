"use strict";


// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];




function primeUltimo (puntuaciones) {

    const puntuacionesCopia = [...puntuaciones]
    
    const totalPuntos = []
    for (const pnt of puntuacionesCopia) {
        let sumaPuntos = 0
    
        for (let i = 0; i < pnt.puntos.length; i++) {
            sumaPuntos += pnt.puntos[i]
        }
    
        totalPuntos.push({
            nombre: pnt.equipo,
            puntosTotales: sumaPuntos
        }) 
    } 
    
    const res = totalPuntos.sort((a, b) => {return b.puntosTotales - a.puntosTotales})
    console.log(`El equipo con mas puntos a sido ${res[0].nombre}, con un total de ${res[0].puntosTotales}
    y el quipo con menos puntos a sido ${res[res.length -1].nombre}, con un total de ${res[res.length -1].puntosTotales}`)
    
}    

primeUltimo (puntuaciones)
 

